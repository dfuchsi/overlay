# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PHP_EXT_NAME="sync"
PHP_EXT_PECL_PKG="sync"
DOCS=( README.md )

USE_PHP="php7-4 php8-0 php8-1"
PHP_EXT_PECL_FILENAME="${PN/pecl-/}-1.1.2.tgz"

inherit php-ext-pecl-r3

KEYWORDS="~amd64 ~x86"

DESCRIPTION="PHP extension for Named and unnamed synchronization objects"
LICENSE="MIT"
SLOT="7"

DEPEND="dev-lang/php[sharedmem]"
RDEPEND="${DEPEND}"
PHP_EXT_ECONF_ARGS=()
S="${WORKDIR}/${PHP_EXT_PECL_FILENAME/.tgz/}"
PHP_EXT_S="${S}"
